﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader
{
  /// <summary>
  /// Генерирование файла через создание процесса, используется внешняя программа, мы указываем путь и файл генерации через аргументы запуска
  /// </summary>
  class DataGeneratorUseExtentionProcess : IDataGenerator
  {
    public void Generate()
    {
      if (File.Exists(GlobalVars.FileParser))
      {
        Console.WriteLine("Starting Process \n");
        var startInfo = new ProcessStartInfo()
        {
          ArgumentList = { "" },
          FileName = GlobalVars.FileParser
        };
        GlobalVars.UseExternalFileLoader = true;
        Console.WriteLine("FilePath - " + startInfo.FileName);
        Console.WriteLine("File is existing - " + File.Exists(startInfo.FileName));

        var process = Process.Start(GlobalVars.FileParser, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, GlobalVars.XmlFileName));

        // ! Почему то не работает, ругается на путь
        //var process = Process.Start(startInfo);

        Console.WriteLine($"Process Started with id: {process.Id}");
        process.WaitForExit();
        Console.WriteLine("Process is finished");

        Console.WriteLine("Start native method");

      }
      else
      {
        Console.WriteLine("file not fount, invalid path to file");
      }
    }
  }
}
