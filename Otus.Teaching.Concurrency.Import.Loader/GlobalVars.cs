﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace Otus.Teaching.Concurrency.Import.Loader
{
  /// <summary>
  /// Отдельный класс для хранения "Божественных объектов", объектов доступных изв всех частей программы, представим что она у нас большая
  /// </summary>
  public static class GlobalVars
  {
    public static string ConfigFile { get; set; } = "Configuration.ini";
    private static FileIniDataParser parser = new FileIniDataParser();
    private static IniData data = GetConfigData();

    private static string fileParser;
    private static string xmlFileName;
    private static int countUserGenerate;
    private static bool useExternalFileLoader = bool.Parse(data["main"]["UseExternalFileLoader"]);
    private static bool databaseIsExist;
    private static string dbName;
    private static string connectiongString;
    public static object _lock = new object();
    private static int countThreads;

    public static void SaveCOnfigFile()
    {
      parser.WriteFile(ConfigFile, data);
    }
    private static IniData GetConfigData()
    {
      if (!File.Exists(ConfigFile))
        File.Create(ConfigFile);
      return parser.ReadFile(ConfigFile);
    }

    /// <summary>
    /// Строка путь до exe файла генериружщего xml файл
    /// </summary>
    public static string FileParser
    {
      get
      {
        if (data["main"][GetMethodName()] == null)
          FileParser = "";
        return string.IsNullOrEmpty(fileParser) ? data["main"][GetMethodName()] : fileParser;
      }
      internal set
      {
        fileParser = value;
        data["main"][GetMethodName()] = fileParser;
        SaveCOnfigFile();
      }
    }

    /// <summary>
    /// Имя xml файла
    /// </summary>
    public static string XmlFileName
    {
      get
      {
        if (data["main"][GetMethodName()] == null)
          XmlFileName = "Default.xml";
        return string.IsNullOrEmpty(xmlFileName) ? data["main"][GetMethodName()] : xmlFileName;
      }
      internal set
      {
        xmlFileName = value;
        data["main"][GetMethodName()] = xmlFileName;
        SaveCOnfigFile();
      }
    }

    private static string GetMethodName(
        [CallerMemberName] string nameMethod = "")
    {
      return nameMethod;
    }

    /// <summary>
    /// Колличество генерируемых личностей
    /// </summary>
    public static int CountUserGenerate
    {
      get
      {
        if (data["main"][GetMethodName()] == null)
          CountUserGenerate = 1000;
        return countUserGenerate == 0 ? Convert.ToInt32(data["main"][GetMethodName()]) : countUserGenerate;
      }
      internal set
      {
        countUserGenerate = value;
        data["main"][GetMethodName()] = countUserGenerate.ToString();
        SaveCOnfigFile();
      }
    }
    /// <summary>
    /// Флаг использования внешнего загрузчика файла
    /// </summary>
    public static bool UseExternalFileLoader
    {
      get
      {
        if (data["main"][GetMethodName()] == null)
          UseExternalFileLoader = false;
        return useExternalFileLoader;
      }
      internal set
      {
        useExternalFileLoader = value;
        data["main"][GetMethodName()] = useExternalFileLoader.ToString();
        SaveCOnfigFile();
      }
    }

    public static string DbName
    {
      get
      {
        if (string.IsNullOrEmpty(dbName))
        {
          if (data["main"][GetMethodName()] == null)
            DbName = "Customers";
          dbName = data["main"][GetMethodName()];
        }

        return dbName;
      }
      internal set
      {
        dbName = value;
        data["main"][GetMethodName()] = dbName.ToString();
        SaveCOnfigFile();
      }
    }
    public static bool DatabaseIsExist
    {
      get
      {
        if (!databaseIsExist)
          databaseIsExist = File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DbName + ".db"));
        return databaseIsExist;
      }
      internal set
      {
        databaseIsExist = value;

      }
    }

    public static int CountThreads
    {
      get
      {
        if (data["main"][GetMethodName()] == null)
          CountThreads = 50;
        return countThreads == 0 ? Convert.ToInt32(data["main"][GetMethodName()]) : countThreads;
      }
      internal set
      {
        countThreads = value;
        data["main"][GetMethodName()] = countThreads.ToString();
        SaveCOnfigFile();
      }
    }

    public static string ConnectingString
    {
      get
      {
        if (string.IsNullOrEmpty(connectiongString))
        {
          if (data["main"][GetMethodName()] == null)
            ConnectingString = @"Server=.\SQLEXPRESS;Database=Testing5;Trusted_Connection=true";
          connectiongString = data["main"][GetMethodName()];
        }

        return connectiongString;
      }
      internal set
      {
        connectiongString = value;
        data["main"][GetMethodName()] = connectiongString.ToString();
        SaveCOnfigFile();
      }
    }
  }
}
