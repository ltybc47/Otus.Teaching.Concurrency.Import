﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader.Menu
{
  /// <summary>
  /// Ворованное, можно не оценивать
  /// </summary>
  public interface IState
  {
    IState RunState();
  }
}
