﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader.Menu
{
  class LoadFileState : IState
  {

    public LoadFileState(IDataLoader loader, IState MainMenuState)
    {
      Loader = loader;
      this.MainMenuState = MainMenuState;
    }

    public IDataLoader Loader { get; }
    public IState MainMenuState { get; }

    public IState RunState()
    {
      Loader.LoadData();
      return MainMenuState;

    }
  }
}
