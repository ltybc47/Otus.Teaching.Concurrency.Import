﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader.Menu
{
  class GenerateFileState : IState
  {
    public GenerateFileState(IDataGenerator loader, IState MainMenuState)
    {
      Generator = loader;
      this.MainMenuState = MainMenuState;
    }

    public IDataGenerator Generator { get; }
    public IState MainMenuState { get; }

    public IState RunState()
    {
      Generator.Generate();
      return MainMenuState;
        
    }
  }
}
