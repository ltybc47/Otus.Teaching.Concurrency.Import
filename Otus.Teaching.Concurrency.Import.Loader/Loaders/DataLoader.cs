﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
  /// <summary>
  /// Загрузчик данных, умеет загружать данные из файла 
  /// </summary>
  /// <typeparam name="T"></typeparam>
  internal class DataLoader<T> : IDataLoader
  {
    public Dictionary<int, List<T>> dictListCustomers = new Dictionary<int, List<T>>();
    private IEnumerable<T> customers;
    private int countThread = 1;
    private string fullPathToFile;
    private IDataParser<T> _dataParser;

    public DataLoader(List<T> list, int countThread)
    {
      customers = list;
      this.countThread = countThread;
    }

    public ICustomerRepository CustomerRepository { get; private set; }

    /// <summary>
    /// загрузка данных из файла, сразу распараллеливаем в отдельные списки по количеству настроенных потоков
    /// </summary>
    public void LoadData()
    {
      dictListCustomers.Clear();
      int i = 0;
      for (i = 0; i < countThread; i++)
        dictListCustomers.Add(i, new List<T>());
      i = 0;
      foreach (var item in customers)
      {
        i = i < countThread - 1 ? i + 1 : 0;
        dictListCustomers[i].Add(item);
      }
      customers = null;

      // В задании сказано что обработкой должна являтся загрузка данных в БД
    }
    /// <summary>
    /// Непосредственно метод парсинга
    /// </summary>
    internal void ParseData()
    {
      //распараллеливание
      List<Thread> threads = new List<Thread>();

      Stopwatch sw1 = new Stopwatch();
      sw1.Start();

      foreach (var item in dictListCustomers)
      {
        // создание задачи
        Thread t = new Thread(() =>
        {
          using (var parser = new DataParser<Customer>(item.Key, (item.Value as List<Customer>), CustomerRepository))
          {
            parser.Parse();
          }
        });
        // запуск задачи
        threads.Add(t);
      }
      foreach (var t in threads)
      {
        t.Start();
      }
      foreach (var t in threads)
      {
        t.Join();
      }
      // ожидание завершения всех задач 
      sw1.Stop();
      Console.WriteLine(sw1.ElapsedMilliseconds.ToString());
    }
    // оставил для себя
    private void ParseDataWithThreadPool()
    {
      //распараллеливание
      WaitCallback dotask;
      WaitHandle[] waitHandles = new WaitHandle[dictListCustomers.Count];

      Stopwatch sw1 = new Stopwatch();
      sw1.Start();

      foreach (var item in dictListCustomers)
      {
        waitHandles[item.Key] = new AutoResetEvent(false);
        // создание задачи
        dotask = (state) =>
        {
          using (var parser = new DataParser<Customer>(item.Key, (item.Value as List<Customer>), CustomerRepository))
          {
            parser.Parse();
          }
          AutoResetEvent are = (AutoResetEvent)state;
          are.Set();
        };
        // запуск задачи
        ThreadPool.QueueUserWorkItem(dotask, waitHandles[item.Key]);
      }
      // ожидание завершения всех задач 
      WaitHandle.WaitAll(waitHandles);
      sw1.Stop();
      Console.WriteLine(sw1.ElapsedMilliseconds.ToString());
    }

    /// <summary>
    /// подключение репозитория в который будем парсить
    /// </summary>
    /// <param name="customerRepository"></param>
    internal void ParseTo(ICustomerRepository customerRepository)
    {
      CustomerRepository = customerRepository;
    }

    internal void ParseDataToMSSQL()
    {
      //распараллеливание
      List<Thread> threads = new List<Thread>();
      Stopwatch sw1 = new Stopwatch();
      sw1.Start();

      foreach (var item in dictListCustomers)
      {
        // создание задачи
        Thread t = new Thread(() =>
        {
          // по заданию имитируем несколько отдельных подключений
          using (var parser = new DataParser<Customer>(item.Key, (item.Value as List<Customer>), new MssqlRepository(ConnectingString: GlobalVars.ConnectingString)))
          {
            parser.Parse();
            parser.SaveChanges();
          }
        });
        threads.Add(t);
      }
      foreach (var t in threads)
      {
        t.Start();
      }
      foreach (var t in threads)
      {
        t.Join();
      }
      // ожидание завершения всех задач 
      sw1.Stop();
      Console.WriteLine(sw1.ElapsedMilliseconds.ToString());
    }
  }
}
