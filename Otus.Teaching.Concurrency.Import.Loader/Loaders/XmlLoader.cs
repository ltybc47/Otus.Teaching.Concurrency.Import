﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.ConcurrentFileLoader.Loaders
{
  class XmlLoader<T> : IDataLoader, IDisposable
  {
    XmlSerializer xmlSer;
    Stream reader;
    public XmlLoader(Task task, string fileParser)
    {
      xmlSer = new XmlSerializer(typeof(T));
      reader = File.OpenRead(fileParser);
    }


    public void Dispose()
    {
      reader.Dispose();
    }

    public void LoadData()
    {
      var ListCustomers = (CustomersList)xmlSer.Deserialize(reader);
      foreach (var item in ListCustomers.Customers)
      {
        Console.WriteLine(item.FullName);
      }
    }
  }
}
