﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.DB;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Menu;
using System;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.Loader
{
  class Program
  {
    private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");


    static void Main(string[] args)
    {

      if (!TryValidateAndParseArgs(args))
        return;

      using var sqlitedbContext = new SQLIteDBContext(GlobalVars.DbName);
      new DbInitializer(sqlitedbContext);
      SqliteCustomersRepository_Singleton.Init(GlobalVars.DbName);

      InitMenu();
    }

    private static void InitMenu()
    {
      int i = 1;
      var menuState = new ConfigurableMenuState();
      menuState.AddMenuItem(i++, new MenuItem() { Text = "Отобразить меню" }, () => menuState);
      menuState.AddMenuItem(i++, new MenuItem() { Text = "Сгенерировать файл используюя внешний генератор, Через создание отдельного процесса" }, () =>
      {
        new DataGeneratorUseExtentionProcess().Generate();
        return menuState;
      });
      menuState.AddMenuItem(i++, new MenuItem() { Text = "Сгенерировать файл используюя встроенный генератор, через подключенную библиотеку" }, () =>
      {
        new DataGeneratorUseNativeMethod().Generate();
        return menuState;
      });
      menuState.AddMenuItem(i++, new MenuItem() { Text = "Загрузить данные напрямую, несколькими потоками, но через Lock" }, () =>
      {

        LoadAndSaveXmlFile(new SqliteRepository(GlobalVars.DbName));
        return menuState;
      });
      menuState.AddMenuItem(i++, new MenuItem() { Text = "Загрузить данные напрямую, несколькими потоками, с попытками" }, () =>
      {

        //что то мне подскажиывает что так делать нехорошо.
        LoadAndSaveXmlFile(new SqliteRepository_v2(new SQLIteDBContext(GlobalVars.DbName)));
        return menuState;
      });
      menuState.AddMenuItem(i++, new MenuItem() { Text = "Загрузить данные в mssql" }, () =>
      {

        LoadAndSaveXmlFile_v2();
        return menuState;
      });

      menuState.AddMenuItem(i++, new MenuItem() { Text = "Exit" }, () => null);

      IState startState = menuState;
      while (startState != null) startState = startState.RunState();
    }

    private static void LoadAndSaveXmlFile_v2()
    {
      // подготовка дессериализатора
      string fullPathToFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, GlobalVars.XmlFileName);
      if (File.Exists(fullPathToFile))
      {
        // Получение списка человеков
        XmlSerializer xmlSer = new XmlSerializer(typeof(CustomersList));
        CustomersList list = (CustomersList)xmlSer.Deserialize(File.OpenRead(fullPathToFile));

        DataLoader<Customer> loaderFrom = new DataLoader<Customer>(list.Customers, countThread: GlobalVars.CountThreads);
        
        loaderFrom.LoadData();
        loaderFrom.ParseDataToMSSQL();
      }
    }

    private static void LoadAndSaveXmlFile(ICustomerRepository repository)
    {
      // подготовка дессериализатора
      string fullPathToFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, GlobalVars.XmlFileName);
      if (File.Exists(fullPathToFile))
      {
        // Получение списка человеков
        XmlSerializer xmlSer = new XmlSerializer(typeof(CustomersList));
        CustomersList list = (CustomersList)xmlSer.Deserialize(File.OpenRead(fullPathToFile));

        DataLoader<Customer> loaderFrom = new DataLoader<Customer>(list.Customers, countThread: GlobalVars.CountThreads);
        
        loaderFrom.LoadData();
        loaderFrom.ParseTo(repository);
        loaderFrom.ParseData();

      }
      else
      {
        Console.WriteLine($"Файл не найден \n {fullPathToFile} \n");
      }
    }


    private static bool TryValidateAndParseArgs(string[] args)
    {
      if (args != null && args.Length > 0)
      {
        for (int i = 0; i < args.Length; i++)
        {
          switch (args[i])
          {
            case "-h":
              viewHelp();
              return false;
              break;
            case "-l":
              if (i < args.Length && args[i + 1].First() != '-')
              {
                GlobalVars.FileParser = args[i + 1];
              }
              else
              {
                Console.WriteLine("ERROR: Path to FileParser is notFound");
                return false;
              }
              i++;  //чтобы перескачить через аргумент ключа -l
              break;
            default:

              viewHelp();
              throw new ArgumentException(message: "invalid enum value", paramName: args[i]);
          }
        }
      }
      if (string.IsNullOrEmpty(GlobalVars.FileParser))
      {
        Console.WriteLine("ERROR: Path to FileParser is notFound, check config file");
        return false;
      }

      return true;
      void viewHelp()
      {

        Console.WriteLine("\n" +
          "-h		: Help" +
          "-l		: Global path to FileLoader utilite"
          );
      }
    }
  }

}