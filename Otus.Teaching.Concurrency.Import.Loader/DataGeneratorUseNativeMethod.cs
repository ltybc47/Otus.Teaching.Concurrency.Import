﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using System;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.Loader
{
  /// <summary>
  /// Генерирование файла используя подключенную библиотеку
  /// </summary>
  class DataGeneratorUseNativeMethod : IDataGenerator
  {
    public void Generate()
    {
      if (File.Exists(GlobalVars.XmlFileName))
        File.Delete(GlobalVars.XmlFileName);

      var program = new Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator(GlobalVars.XmlFileName, GlobalVars.CountUserGenerate);
      program.Generate();
    }
  }
}