﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
  public class DataParser<T> : IDisposable, IDataParser<T> where T : new()
  {
    List<T> listObj = new List<T>();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <param name="value"></param>
    /// <param name="repository"></param>
    public DataParser(int id, List<T> value, ICustomerRepository repository)
    {
      Id = id;
      this.listObj = value;
      Repository = repository;
    }
    public int Id { get; }
    public ICustomerRepository Repository { get; }

    public void Dispose()
    {
      Repository.Dispose();
    }

    /// <summary>
    /// парсинг списка в бд
    /// </summary>
    /// <returns></returns>
    public T Parse()
    {
      foreach (var item in listObj)
      {
        if (item != null)
          Repository.AddCustomer((item as Customer));
      }

      Console.WriteLine(Id.ToString());
      //Console.WriteLine(string.Join("\n", listObj.Select(x => Id.ToString() + x.ToString())));
      // по контракты прописанному в интерфейсе мы должны что то вернуть, но по логике мы ничего возвращать не должны, парсер парсит данные в БД
      return listObj.FirstOrDefault();
    }

    int countTrySave = 5;
    public void SaveChanges()
    {
      try
      {
        Repository.SaveChanges();

      }
      catch (Exception e )
      {
        if (countTrySave > 0)
        {
          countTrySave--;
          Thread.Sleep(1000);
          Repository.SaveChanges();
        }
        else
        {
          throw e;
        }
      }
    }
  }
}
