﻿using Otus.Teaching.Concurrency.Import.DataAccess.DB;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
  public class SqliteRepository : ICustomerRepository
  {
    private string dbName;

    public SqliteRepository(string dbName)
    {
      this.dbName = dbName;
    }

    public void AddCustomer(Customer customer)
    {
      using(var db = new SQLIteDBContext(dbName))
      {
        db.Customers.Add(customer);
        db.SaveChanges();
      }
    }

    public void Dispose()
    {
      
    }

    public void SaveChanges()
    {
      
    }
  }
}
