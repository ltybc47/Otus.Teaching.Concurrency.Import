using System;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
  public class SqliteRepository_v2 : ICustomerRepository
  {
    public SqliteRepository_v2(DbContext context)
    {
      Context = context;
    }

    public DbContext Context { get; }

    public void AddCustomer(Customer customer)
    {
      Context.Set<Customer>().Add(customer);
    }

    public void SaveChanges()
    {
      Context.SaveChanges();
    }
    /// <summary>
    /// sqlite ��� SaveChange() ��������� ������ �� ����, ���� ��� ������ ������ ��� ��� ���������� �� ������ ��� ����� ��������� � �����
    /// ��� ������� ��������� � ����� ����� ����������� � ����� ������.
    /// </summary>
    public void Dispose()
    {
      Context.Dispose();
    }

  }
}