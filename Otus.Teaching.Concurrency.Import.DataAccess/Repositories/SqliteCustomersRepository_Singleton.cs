﻿using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
  public static class SqliteCustomersRepository_Singleton
  {
    private static ICustomerRepository repository;
    private static string dbName;

    public static ICustomerRepository GetRepository()
    {
      if (repository == null)
      {
        repository = new SqliteRepository(dbName);
      }
      return repository;
    }
    public static void Init(string DbName)
    {
      dbName = DbName;
    }
  }
}
