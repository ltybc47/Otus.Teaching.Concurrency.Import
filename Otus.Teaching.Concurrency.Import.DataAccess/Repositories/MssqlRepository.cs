﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.DataAccess.DB;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
  public class MssqlRepository : ICustomerRepository
  {
    private MSSQLContext db;
    private int countErrors;

    public MssqlRepository(string ConnectingString)
    {
      var optionsBuilder = new DbContextOptionsBuilder<MSSQLContext>();
      var options = optionsBuilder
              .UseSqlServer(ConnectingString)
              .Options;

      db = new MSSQLContext(options);
      
    }

    public void AddCustomer(Customer customer)
    {
      try
      {
        db.Customers.Add(customer);
        countErrors = 0;
      }
      catch (System.Exception e)
      {
        if (countErrors < 5)
        {
          countErrors++;
          System.Console.WriteLine($"Поток {Thread.GetCurrentProcessorId()}: Ошибка записи №{countErrors}");
          AddCustomer(customer);
        }
        throw e;

      }
    }

    public void Dispose()
    {
      db.Dispose();
      Locks.FirstInit = true;
    }

    public void SaveChanges()
    {
      try
      {
        db.SaveChanges();
        countErrors = 0;
      }
      catch (System.Exception e)
      {
        if (countErrors < 5)
        {
          countErrors++;
          System.Console.WriteLine($"Поток {Thread.GetCurrentProcessorId()}: Ошибка записи №{countErrors}");
          SaveChanges();
        }
        throw e;

      }
    }
  }
}
