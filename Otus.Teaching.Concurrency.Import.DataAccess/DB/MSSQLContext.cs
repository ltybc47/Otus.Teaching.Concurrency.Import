﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.DB
{
  public class MSSQLContext : DbContext
  {

    private string databaseName;

    public DbSet<Customer> Customers { get; set; }

    public MSSQLContext(DbContextOptions options) : base(options)
    {
      lock (Locks.mssqlLock)
      {

        if (Locks.FirstInit)
        {
          Database.EnsureDeleted();
          Database.EnsureCreated();
          Locks.FirstInit = false;
        }
      }
    }
  }
}
