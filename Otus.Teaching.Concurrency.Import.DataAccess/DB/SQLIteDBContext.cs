﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.DB
{
  public class SQLIteDBContext : DbContext
  {
    private string databaseName;

    public DbSet<Customer> Customers { get; set; }

    public SQLIteDBContext(string databaseName)
    {
      this.databaseName = databaseName;
        if (!File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, databaseName + ".db")))
        {
          Database.EnsureCreated();
        }
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.UseSqlite($"Filename={databaseName}.db");
    }
  }
}
