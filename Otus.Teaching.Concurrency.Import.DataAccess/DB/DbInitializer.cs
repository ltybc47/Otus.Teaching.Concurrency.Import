﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.DB
{
  public class DbInitializer
  {
    private SQLIteDBContext sqlitedbContext;

    public DbInitializer(SQLIteDBContext sqlitedbContext)
    {
      this.sqlitedbContext = sqlitedbContext;
      sqlitedbContext.Database.EnsureDeleted();
      sqlitedbContext.Database.EnsureCreated();
    }
  }
}
