using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
  public interface ICustomerRepository : IDisposable
  {
    void AddCustomer(Customer customer);
    void SaveChanges();
    void Dispose();
  }
}